const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SongSchema = new Schema({
    title:         {type: String, required: true, trim: true},
    duration:      {type: Number, required: true},
    songUrl:       {type: String, required: true},
    artist_band:   {type: Schema.Types.ObjectId, ref: "artist", required: true},
    cover_image:   {type: Buffer, required: false}
});

const SongModel = mongoose.model('songs', SongSchema); 
module.exports = SongModel; 