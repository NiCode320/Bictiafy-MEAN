const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const validator = require("validator"); 
const bcrypt = require("bcryptjs");

const UserSchema = new Schema({ //se instancia el objeto schema 
    name:        {type: String, required: true, trim: true},
    lastname:    {type: String, required: true, trim: true},
    username:    {type: String, required: true, trim: true},
    preferences: {type: Array,  required: true, trim: true},
    email:       {type: String, required: true, trim: true, 
                    validate(value){
                        if(!validator.isEmail(value)){
                            throw new Error("Email is not valid"); 
                        }
                    },
                },
    password:    {type: String, required: true, 
                 validate(value){
                    if(value == "password" || value == "PASSWORD" ||
                       value == "123456"   || value == "123456789"){ 
                        throw new Error("Try a more complex password please")
                    }
                 }},

    tokens: [
        {
            token: {type: String, required: true}
        }
    ]
});

UserSchema.pre("save", async function(req,res,next){
    if(this.isModified("password")){
        this.password = await bcrypt.hash(this.password, 10);
    }
    next(); 
})

const UserModel = mongoose.model('Usuario', UserSchema); 
module.exports = UserModel;