const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const validator = require("validator");
const bcrypt = require("bcryptjs");

const ArtistSchema = new Schema({
    name:            {type: String, required: true, trim:true},
    description:     {type: String, required: true, trim:true},
    profilePicture:  {type: String, required: true, trim:true},
    contact:         {
                        phone1:     {type: Number, required: false},
                        phone2:     {type: Number, required: false},
                        facebook:   {type: String, required: false}, 
                        instagram:  {type: String, required: false},
                        spotify:    {type: String, required: false},
                        soundcloud: {type: String, required: false}
                     },          
    email:           {type: String, required: true, trim: true,
                     validate(value){
                        if(!validator.isEmail(value)){
                            throw new Error("Email not valid. Try again")
                        }
                     }},
    password:        {type: String, required: true, 
                     validate(value){
                        if(value == "password" || value == "PASSWORD" ||
                           value == "123456"   || value == "123456789"){ 
                                throw new Error("Try a more complex password please")
                        }
                     }},
    tokens: [
                    {
                        token: {type: String, required: true}
                    }
            ]
},{
    timestamps: true
});

ArtistSchema.virtual("albums", {
    ref: "albums", 
    localField: "name",
    foreignField: "_id"
})

ArtistSchema.virtual("songs", {
    ref: "songs", 
    localField: "name",
    foreignField: "_id"
})

ArtistSchema.pre("save", async function(next){
    if(this.isModified("password")){
        this.password = await bcrypt(this.password, 10); 
    }
    next();
})

const ArtistModel = mongoose.model('artists', ArtistSchema); 
module.exports = ArtistModel; 
