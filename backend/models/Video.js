const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
    title:       {type: String, required: true, trim: true},
    songs:       {type: String, required: true, trim: true},
    duration:    {type: Number, required: true, trim: true},
    releaseYear: {type: Number, required: true, trim: true},
    coverImage:  {type: String, required: true, trim: true},
    artista:     {type: Schema.ObjectId, ref: 'artist', required: true}
});

AlbumSchema.virtual("album_songs", { 
    ref: "songs",
    localField: "_id", 
    foreignField: "album"
})

const AlbumModel = mongoose.model('albums', AlbumSchema);
module.exports = AlbumModel; 