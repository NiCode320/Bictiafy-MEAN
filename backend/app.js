const express = require("express");
const app     = express(); 
const port    = process.env.PORT || 3006;
      
app.use(express.json());

require("./database/mongoDB")

//Using routes
const userRoutes = require("./routes/userRoutes"); 
app.use("/user", userRoutes);

const artistRoutes = require("./routes/artistRoutes"); 
app.use("/artists",   artistRoutes);

const albumRoutes  = require("./routes/albumRoutes");
app.use("/albums",    albumRoutes);

const songRoutes   = require("./routes/songRoutes"); 
app.use("/canciones", songRoutes);


app.listen(port, () => {
    console.log(`Connected on port ${port}`)
})