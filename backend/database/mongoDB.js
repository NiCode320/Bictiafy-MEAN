const mongoose = require("mongoose");
mongoose.set('useCreateIndex', true);

mongoose.connect(process.env.MONGO_URI,
    { useUnifiedTopology: true, useNewUrlParser: true })
    .then(db => console.log("MongoDB is connected"))
    .catch(err => console.log(">>DATABASE ERROR: ",err));

module.exports = mongoose;








