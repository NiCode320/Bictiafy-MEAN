const express = require("express"); 
const router = express.Router();
const albumCtrl = require("../controllers/albumCtrl"); 

router.post   ("/create",     albumCtrl.createAlbum); 
router.get    ("/albums",     albumCtrl.readAlbums); 
router.get    ("/albums/:id", albumCtrl.readOneAlbum); 
router.patch  ("/album/:id",  albumCtrl.updateAlbum); 
router.delete ("/album/:id",  albumCtrl.deleteAlbum);

module.exports = router; 