const express = require("express"); 
const router = express.Router();
const artistCtrl = require("../controllers/artistCtrl")

router.post   ("/create"     , artistCtrl.createArtist); 
router.get    ('/artists'    , artistCtrl.readArtists);
router.get    ("/artist/:id" , artistCtrl.readOneArtist);
router.patch  ("/artist/:id" , artistCtrl.updateArtist);
router.delete ("/artist/:id" , artistCtrl.deleteArtist);

module.exports = router;