const express = require("express");
const router = express.Router();

const songCtrl = require("../controllers/songCtrl");

router.post   ( "/create",   songCtrl.createSong);
router.get    ( "/songs",    songCtrl.readSong);
router.get    ( "/song/:id", songCtrl.readSingleSong);
router.patch  ( "/song/:id", songCtrl.updateSong);
router.delete ( "/song/:id", songCtrl.deleteSong); 

module.exports = router; 
