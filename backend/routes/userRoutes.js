const express = require("express");
const router = express.Router(); 

const userCtrl = require("../controllers/userCtrl");
const auth = require("../middleware/userAuthentication")

router.post('/register',    auth, userCtrl.signUp); 
router.get('/profile',      auth, userCtrl.readProfile); 
router.put('/:id',          auth, userCtrl.updateProfile);
router.use('/:id',          auth, userCtrl.deleteProfile);

// login and logout
router.post("/login",      auth, userCtrl.login);
router.post("/logout",     auth, userCtrl.logout);
router.post("/logout/all", auth, userCtrl.logoutAll);

module.exports = router;  